#!/usr/bin/env python3

# Mi primera variable

mensaje = "Hola mundo"

print(mensaje)

# Python es de tipado dinamico, puede cambiar su tipo de valor
# en tiempo de ejecucion
myvar = 5
myvar = "string"

# Obtener el tipo de una variable
print(type(myvar))
